---
metadonnees:
- description: Libræ s'engage, dès la conception de ses sites, à minimiser le plus possible les impacts de ses réalisations.
---

# Engagements

Au fil des années, Internet est devenu un espace de communication et d'information toujours plus difficile à contourner et semblant solutionner nombre de problèmes. Mais ce secteur n’est pas dénué d’impacts environnementaux et sociaux. 

Libræ s'engage, dès la conception de ses sites, à minimiser le plus possible les impacts de ses réalisations.

## Écoconception

Le numérique a un impact grandissant sur l’environnement de par ses émissions de gaz à effet de serre (*5,2 % des émissions de la France*), sa consommation électrique (*5,6 % par an à l’échelle mondiale*) ou encore sa consommation d’eau douce potable (*10,2 % de la consommation de la France*). 

l'écoconception web se présente comme **une réponse en vue de produir des outils numériques efficients** à moindre impact.

Quelques bonnes pratiques :
: éliminer les fonctionnalités non essentielles (*70 %*) voire inutiles (*40%*),
: limiter la complexité des pages web,
: limitation des requêtes effectuées entre le navigateur et le serveur,
: réduire l'utilisation des médias (images, videos) et optimiser leur poid le cas échéant.

## Accessibilité

D’après une étude INSEE de 2007, les personnes en situation de handicap représentent entre 10 et 20 % de la population. 

L'accessibilité se présente comme **une réponse afin de rendre le web compréhensibles et utilisables** pour les personnes en situation de handicap (mais pas que).

Quelques bonnes pratiques :
: Doter les medias porteur d'informations d'alternatives textuelles,
: Contraster les couleurs de fonds et de textes pour que ceux-ci soient lisibles,
: Restituer convenablement les informations aux technologies d'assistance,
: Structurer convenablement l'information sur chaque page web.


## Illectronisme

D’après une étude de l’INSEE de 2019, “une personne sur six n’utilise pas Internet et plus d’une personne sur trois manque de compétences numériques de base”. Internet est aujourd’hui un outil permettant l’accès à l’information, des connaissances, des interactions sociales et même des droits individuels. 

Lutter contre la fracture créée par les services numériques  se présente comme **une réponse afin de rendre des droits aux internautes et le web plus utilisable**.

Quelques bonnes pratiques :
: faciliter l'accès à l’information au sein des pages,
: présenter le contenu de manière intuitive et intelligible,
: penser aux personnes ne disposant pas d'un reseau performant.

## Pourquoi tenir compte de ces problématiques ?

On pourrait trouver plein d'argument marketing mais la seule vrai raison est que la prise en compte de ce problématiques est juste une affaire d'engagement et de morale. Alors, on se lance ?

[*Je me lance*](mailto:mathieu@librae.fr?subject=%5Blibræ.fr%5D%20Travaillons%20ensemble%20!&body=Bonjour%20Mathieu%20!%0A%0A){title="Je me lance, envoyer un courriel depuis ma boite mail" target="_self"}