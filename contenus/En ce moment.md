---
metadonnees:
- description:  Bienvenue sur ma page en ce moment qui offre une vue d'ensemble de mes activités du moment.
---

# En ce moment

Bienvenue sur ma page *en ce moment* qui offre une vue d'ensemble des activités qui m'occupent.

## <i aria-hidden="true">🧑‍💻</i> Mes projets web

Camping du Razay
: Création d'un site web permettant la présentation des différents hébergements proposés par le camping. 

Animaction 
: Refonte et modernisation du site web de l'association Animaction, située à Port-Saint-Père.
: Avec : [Anne-Sophie](https://little-sun.fr/){target="_blank"}, [Frank](https://www.franckdavid.fr/){target="_blank"} et [Lise](https://www.liserousseau.fr/){target="_blank"} 

Amicale laïque Saint Sébastien 
: Refonte et modernisation du site de l'amicale.
: Avec [Lise](https://www.liserousseau.fr/){target="_blank"}.

Association RAFUE 
: Refonte du site web de l'association pour sortir de la solution *YesWiki*, trop complexe à gérer pour les membres.

## <i aria-hidden="true">🧑‍🏫</i> Mes formations

La Colinière
: Chaque mardi et mercredi, j'interviens auprès des premières années sur le bloc 1 et 3 du BTS,
: Le projet fil rouge de ce semestre sera la refonte de l'application web ministage.

## <i aria-hidden="true">⚗️</i> Mes expériences

- Un générateur de site depuis du [Markdown]{lang="_en"}, dans un esprit FALC (Facile À Lire et à Comprendre), avec des ateliers de contribution ouverts à toustes.
- La synthèse de mes écrits de ces dernières années, pour clarifier mes idées et de faire preuve d'une plus grande transparence sur mon travail.

## <i aria-hidden="true">🤔</i> Mes reflexions

- Je dois mieux rémunérer mon travail sur les projets qui me sont confiés et notamment pour les phases d'analyses, de conceptions, et lors des retours des tests utilisateurices.
- J'aimerais proposer des ateliers afin d'acculturer les gens au numérique, à ses impacts et comment lutter contre.
- J'aimerais proposer des formations express pour permettre au gens de concevoir et gérer rapidement leur propre site web.

<footer class="mtm">

*Page inspirée du site [encemoment.site](https://encemoment.site/){target="_blank"} découvert grâce à une publication de [Frédéric Couchet](https://couchet.org/){target="blank"} sur Mastodon.*

*Le concept de “now page” a été initié par Derek Sivers en 2015. Ce concept est simple : alors qu’une page “À propos” renseigne des informations générales sur la personne qui entretient le site, la page “En ce moment” apporte des précisions sur ce qui se passe actuellement dans sa vie.*

</footer>