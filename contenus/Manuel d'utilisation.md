---
metadonnees:
- description: Ce manuel d'utilisation décrit la façon dont je travaille et fonctionne. J'espère qu'il aidera à mieux cerner qui je suis dans mon quotidien.
---

# Mon manuel d'utilisation

Ce manuel d'utilisation décrit la façon dont je travaille afin de se donner les meilleures chance dans nos futures collaborations.

Conditions dans lesquelles j'aime travailler
: J'aime travailler dans des environnements calmes et sans interruptions.
: J'aime travailler de chez moi, au [Grand Bain](https://legrandbain.coop/ "Visiter le site du Grand Bain (ouverture dans une nouvelle fenêtre)"){target="_blank"} ou tout autre endroit où j'ai mes marques et un coin de pièce (au sens propre).
: J'aime travailler au rythme de ma famille pour avoir mon temps libre avec elle.

Les horaires durant lesquelle j'aime travailler
: Je commence mon travail vers 9 heures pour finir vers 16 heures (17 heures maximum au besoin).
: Je ne travaille ni les week-end, ni les mercredis (sauf rares exceptions). 
: Je me sens plus efficace le matin et plutôt en début de semaine.

La meilleure façon de me contacter
: Par <b lang="en">email</b> principalement.
: Par téléphone, en priorisant le SMS, entre 9h et 16h.

Ce dont j'ai besoin
: J'aime faire les premiers rendez-vous en physique, comprendre les problématiques pour entamer une réfléxion sur le projet qu'on souhaite me confier.
: J'ai besoin de temps pour bien aller en profondeur dans mes réfléxions pour m'assurer que celle-ci est optimale.
: J'ai besoin que les choses soient logiques et cadrées techniquement.

Les choses avec lesquelles j'ai du mal
: J'ai du mal lorsque j'ai beaucoup de chose à gérer ou que les choses s'accumulent, cela me cause du stress, une volontée de replie sur moi-même voire des insomnies.
: J'ai du mal à dire non et cela peut parfois me jouer des tours.
: J'ai du mal avec les conflits, les injustices et les situations dissonantes.

Les choses que j'aime
: J'aime passer du temps avec ma famille.
: J'aime apprendre et me documenter sur plein (trop) de sujets: numériques, politiques, sociaux, etc..
: J'aime rencontrer de nouvelles personnes (lors de mes passages à l'Ouvre-Boîtes notamment), découvrir des parcours de vie et des projets inspirants.
: J'aime parler numérique avec mes collegues, imaginer des solutions sobres et résilientes.
: J'aime inventer des utopies et des futures désirables.

Autres choses à savoir à propos de moi
: Je préfère le tutoiement, qui me semble plus naturel.
: J'ai de forte valeurs sociales, écologiques et éthiques. Celles-ci dictent beaucoup de mes décisions. 
: Je suis hypersensible et haut potentiel (les résultats de mon WAIS 4 n'informant ni ne confirmant cela, je fais confiance à mon autodiagnostique)


<footer class="mtm">

*Page inspirée par ma lecture du manuel de [Thomas Parisot](https://détour.studio/manuel/){target="_blank"} et [Cassie Robinson](https://cassierobinson.medium.com/a-user-manual-for-me-d3a851fbc694){target="_blank"}.*

*Il est possible que mon manuel ne soit pas complet. Si tu me connais et que tu penses que quelque-chose de significatif manque à celui-ci, n'hésite pas à me le signifier.*

</footer>