<nav role="navigation">
    <p>
        <small>
            Accès rapide&nbsp;:<wbr>
            <a href="#top">vers le haut de la page</a><wbr>
            <a href="/">vers la page d'accueil</a>
        </small>
    </p>
</nav>


<small>
Me contacter par [courriel [(Nouvelle fenêtre)]{class="sr-only"}](mailto:mathieu@librae.fr?subject=Travaillons%20ensemble%20!){target="_blank"}
<wbr>
Me suivre sur [Mastodon [(Nouvelle fenêtre)]{class="sr-only"}](https://mastodon.scop.coop/@mathieuvigou){rel="me" target="_blank"}
</small>
    
<small>
Libræ est une activité portée par [L'Ouvre-Boîtes [(Nouvelle fenêtre)]{class="sr-only"}](https://www.ouvre-boites.coop/){target="_blank"}
et membre du collectif [Translucide [(Nouvelle fenêtre)]{class="sr-only"}](https://translucide.net){target="_blank"}.
</small>


<small>
    *Ce site a été conçu afin de réduire ses impacts socio-environnementaux: émissions de Co2e, obsolescence de votre matériel et plus grande accessibilité. [Site web conçu sans IA](/ia.html)*
</small>


<small>
    [Mentions légales](/mentions-légales.html)
</small>
