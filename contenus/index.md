---
metadonnees:
- description: Création de sites web à Nantes. Libræ applique les bonnes pratiques d'écoconception et d'accessibilité pour un web moins irresponsable.
---

::: {.mw1140p .h1 .mxa .pxxs}

Le numérique contribue au *dérèglement climatique,* à l'*aliénation sociale*, à l'*invisibilisation des minorités*, à l'*exploitation humaine*.

:::


::: {.mw1140p .mxa}

:::::: {.mw570p .mxr .mym}

![Photo de <a href="https://unsplash.com/fr/@dulhiier?utm_content=creditCopyText&utm_medium=referral&utm_source=unsplash">Nastya Dulhiier</a>](site/media/network.png){alt="Photographie spaciale d'une ville durant la nuit" loading="lazy"}

::::::

:::::: {.pxxs .statistiques}

## Des chiffres bien réels pour un secteur dématérialisé.

::::::::: {.flex .rule .mxr}

- [**1400 M** <br><small>Tonnes de GES.</small>]{title="1400 millions de tonnes de gaz à effet de serre"} 
- [**11,526 M** <br><small>Personnes concernées par l'illectronisme.</small>]{title="11,526 millions de personnes concernées par l'illectronisme"} 
- [**40 k** <br><small>enfants exploités dans des mines.</small>]{title="40000 enfants exploités dans des mines"}

:::::::::

::::::


:::::: {.mw570p .mxr .pxxs}

<!-- [Plus de chiffres](./){target="_self"} -->

::::::

:::::: {.flex .flex-2 .mym}

![Selon Amnesty International, <a href="https://www.amnesty.fr/actualites/republique-democratique-du-congo-enfants-cobalt-face-cachee-de-nos-batterie" target="_blank">40 000 enfants esclaves travaillent dans des mines illégales</a> en République Démocratique du Congo](site/media/mineur.png  "Un jeune garçon dans la mine de Shinkolobwe au sud-est de la République démocratique du Congo."){alt="Un jeune garçon dans la mine de Shinkolobwe au sud-est de la République démocratique du Congo." class="mtm" loading="lazy"}

![Illustration par <a href="https://www.dillonmarsh.com" target="_blank">Dillon Marsh</a> d'une mine de terres rares profonde de 600 mêtres. La sphère au centre représente les 4,1 millions de tonnes excavés, permettant de se rendre compte du faible rendement de cette matière première.](site/media/cuivre.png  "Sphère de cuivre au centre d'une mine à ciel ouvert"){alt="Sphère de cuivre au centre d'une mine à ciel ouvert" loading="lazy"}

::::::

:::


::: {.mw1140p .mxa .pxxs}

## Et maintenant que l'on sait&nbsp;?

:::::: {.mw570p .mxr}

Aujourd’hui, *la numérisation de nos sociétés et le technosolutionnisme présentent plus d’aspects négatifs que positifs*. Ceux-ci ne permettent pas de répondre aux enjeux environnementaux et de droits humains. La question à se poser est donc: **avez-vous vraiment besoin d'un site web ?** 

Parfois, la réponse peut-être oui, et, dans ce cas, autant faire de son mieux, maintenant que l'on sait&nbsp;!

::::::::: {.keywords}

**Sur-mesure.** **Efficients.** **À moindre impact.**

::::::::: 

Depuis 2019, **Libræ s'investit en faveur d'un numérique désirresponsable**.

[Des services sur-mesure](services.html){title="Des services sur-mesure: découvrir mes prestations" target="_self"}

::::::

:::


> Chaque octet à un impact sur le monde réel. Il est temps de questionner nos besoins numériques&nbsp;!


::: {.mw1140p .mxa .pxxs}

:::::: {.mxr .mw570p}

**Le numérique désirresponsable s'inscrit dans la réduction de l'irresponsabilité du numérique** en ayant à l'esprit que numériser contribuera à péreniser ce secteur dont les effets sont plus négatifs que positifs.

::::::

:::::: {.flex .flex-2 .mym}

![Le bureau sur lequel sont imaginés les sites web sur lesquels je travaille et peut-être, bientôt, le vôtre.](site/media/bureau.png "Un bureau avec un double écran et, posé devant, un clavier et une souris; et, tout à droite, un micro"){alt="Un bureau avec un double écran et, posé devant, un clavier et une souris; et, tout à droite, un micro"}


::::::::: {.mtl}

![Photo de <a href="https://unsplash.com/fr/@claybanks?utm_content=creditCopyText&utm_medium=referral&utm_source=unsplash">Clay Banks</a>](site/media/hello.png){alt="Une main tenant un téléphone portable sur lequel il est écrit: Hello world"}

:::::::::

::::::

:::::: {.mw570p}

## Que pouvez-vous attendre d'un site désirresponsable&nbsp;?

::::::

:::::: {.mxr .mw570p .marker-as-tags .li-marged}

1. Un site peu gourmands en ressources matérielles avec des temps de réponses rapides pour allonger la durée de vie des équipements numériques.
3. Un site qui intègre les bonnes pratiques d'écoconception et d'accessibilité web.
2. Un site qui vous ressemble et correspond à vos besoins pour réduire votre charge mentale lors de sa prise en main.

::::::

:::

::: {.faq .mym}

<details>
<summary title="Est-ce que c'est toujours aussi brut ?">*Est-ce que c'est toujours aussi brut ?* <span class="marker" aria-hidden="true"></span></summary>
<div class="mw1140p pxxs">
<p>**Non, rassurez-vous**. Ce site a été développé dans un *esprit brutaliste*. Pour le reste, Libræ collabore avec des graphistes qui sont là pour faire ressortir votre identité graphique dans votre site.</p>
</div>
</details>

<details>
<summary title="Est-ce que c'est plus cher ?">*Est-ce que c'est plus cher ?*<span class="marker" aria-hidden="true"></span></summary>
<div class="mw1140p pxxs">
<p>*Les projets sont conçus selon vos besoins* et seules les fonctionnalités essentielles y sont mises en place. Ainsi, **les projets ne coutent pas plus cher !**</p>
</div>
</details>

:::

::: {.mw1140p .mxa .mym .pxxs}

:::::: {.mw480p .pts}

## Libræ

*Libræ est une activité imaginée et développée par Mathieu Vigou Didierjean, développaire web [full-stack](https://fr.wikipedia.org/wiki/D%C3%A9veloppeur_full_stack){lang="_en" target="_blank"}, membre du collectif [Translucide](https://translucide.net/){target="_blank"} et sociétaire au sein de [l'Ouvre-Boîtes](https://www.ouvre-boites.coop/){target="_blank"}.*

*Que vous souhaitiez que l'on travaille ensemble ou discuter écoconception, accessibilité ou numérique désirresponsable autour d'une boisson chaude, vous pouvez me contacter par mail.*

[*Envoyer un courriel*](mailto:mathieu@librae.fr){title="Envoyer un courriel - ouverture dans la boîte mail" target="_self"}

::::::

:::::: {.mw570p .mxr .mym}

<nav role="navigation" class="tdm">

<small>[Table des matières]{.smallcaps .letterspacing .monospace}</small>

- [*À propos*](à-propos.html){target="_top"}
    - [*Manuel d'utilisation*](manuel-d'utilisation.html){target="_top"}
    - [*En ce moment*](en-ce-moment.html){target="_top"}
    - [*Notes hebdomadaires*](notes/){target="_top"}
- [*Engagements*](engagements.html){target="_top"} 
    - [*Manifeste IA*](ia.html){target="_top"} 
- [*Services*](services.html){target="_top"}
- [*Articles*](articles/){target="_top"}
- [*Contact*](mailto:mathieu@librae.fr?subject=%5Blibræ.fr%5D%20Travaillons%20ensemble%20!&body=Bonjour%20Mathieu%20!%0A%0A){title="Contact - ouverture dans la boîte mail" target="_blank"}

</nav>

::::::

:::



