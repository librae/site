---
metadonnees:
- description: Mentions légales
---

# Mentions légales

En vertu de l'article 6 de la loi n° 2004-575 du 21 juin 2004 pour la confiance dans l'économie numérique, il est précisé aux utilisateurices du site librae.fr l'identité des parties prenantes de sa réalisation et de son suivi.

**Propriété et création**
: Libræ est une activitée créée par Mathieu Vigou Didierjean et développée au sein de la Coopérative d'Activité et d'Emploi l'Ouvre-Boîtes, 20 allée de la Maison Rouge 44000 Nantes. [Mentions légales](https://www.ouvre-boites.coop/mentions-legales  "Lire les mentions légales de l'Ouvre Boîtes (Nouvel onglet)"){target="_blank"}

**Responsable de publication**
: Mathieu Vigou Didierjean, mathieu *at* librae.fr 

**Hébergement**
: Ce site web est hébergé par [Infomaniak](https://www.infomaniak.com/fr "Visiter le site (Nouvel onglet)"){target="_blank"}, 26 Avenue de la Praille 1227 Carouge / Genève Suisse.

**Informations complémentaires**
: librae.fr été conçu afin de réduire au maximum ses impacts socio-environnementaux : réduction des émissions de Co2e, allongement de la durée de vie de votre matériel et plus grande accessibilité.
: Par respect pour votre vie privée, ce site n'utilise pas de cookies à caractère personnels et n'effectue aucune statistique de suivi.