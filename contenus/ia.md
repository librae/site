---
metadonnees:
- description: Rien sur ce site n'a été généré par une IA
---

# IA

Rien sur ce site, y compris son code source, n'a été rédigé par, ou avec, une IA. 

Aucun des sites réalisés par Libræ ne contient du code source généré par, ou avec, une IA.

*Les IA sont des solutions numériques contribuant à l'accroissement des emissions carbones du numérique, à la dégradation des relations sociales, à l'exploitation humaine et à la spoliation artistique.*


