---
metadonnees:
- description: Je conçois et développe des sites web, sur mesure, sobre et à faible impacts
---

# Bonjour, je m'appelle Mathieu. <i aria-hidden="true">👋</i>

J'ai à cœur de concevoir et développer des **sites web sur mesure, sobres et à faible impacts**, en accord avec mes valeurs, **dans une démarche d'écoconception et d'accessibilité**, en statique ou en dynamique, **pour des entrepreuneureuses** ou **des associations** évoluant dans le milieu de l'ESS (Économie Sociale et Solidaire), ancrées localement (comme des centres sociaux culturels) et/ou engagées sur des thématiques qui m'animent.

## Ma démarche

En 2019, je crée Libræ au sein de la Coopérative d’Activité et d’Emploi l’[Ouvre-Boîtes](https://www.ouvre-boites.coop/ "Visiter le site (Ouverture dans un nouvel onglet)"){target="_blank"}, rejoins le collectif [Translucide](https://www.translucide.net "Visiter le site (Ouverture dans un nouvel onglet)"){target="_blank"}, avec l'envie de me consacrer à un développement plus respectueux de mes valeurs écologiques et éthiques. Au fil du temps, de mes lectures et de mes rencontres, ma vision de ce que devrait être le Numérique s'affine et l'approche de mon travail évolue.

## Mon approche

1. Ne pas numériser un besoin lorsque cela est possible.
2. Concevoir le web avec cohérence grâce à l'écoconception et l'accessibilité.
3. Inssufler mes valeurs dans les projets qui me sont confiés.
4. Sensibiliser aux impacts socio-environnementaux du Numérique.
5. Promouvoir des solutions numériques sobres, frugales et résilientes

## Mes actions

Portant de fortes valeurs écologiques et éthiques, celles-ci s'incarnent dans mon quotidien comme au travail.

Ce que je fais déjà
: Adopter un mode de vie vegan.
: Entreprendre au sein d'un Coopérative d'Activité et d'Emploi.
: Avoir un compte au Crédit Coopératif.
: Me fournir en énergie chez Enercoop et Ilek.
: M'assurer auprès de la MAIF.
: Prioriser la réparation et l'achat d'occasion.
: Abandonner la voiture personnelle au profit de l'autopartage ([Citiz](https://citiz.coop/){target="_blank"}).
: Limiter mes déplacements en voiture à 5000 Kilomètres par an.
: Prioriser le train pour mes déplacements.
: Controler annulement mon impact carbonne.
: Utiliser des logiciels libres sur <em lang="en">smartphone</em> et ordinateur.
: Supprimer mon compte Gmail pour finaliser ma *dégooglelisation*.

Ce qu'il me reste à améliorer
: Contribuer financièrement à des associations dont je partage les valeurs.
: Passer ma certification *développer des sites web accessible conforme au RGAA*.
: *Et surement plein d'autres choses.* [😇]{aria-hidden="true"}


::: {.mw570p .mym}

## En savoir plus

<nav role="navigation" class="tdm">

- [*Manuel d'utilisation*](manuel-d'utilisation.html){target="_top"}
- [*En ce moment*](en-ce-moment.html){target="_top"}
- [*Notes hebdomadaires*](notes/){target="_top"}

</nav>

:::