---
metadonnees:
- description: Des articles sur des sujets qui m’occupent et me préoccupent dans le cadre mon travail (écoconception, accessibilité, économie sociale et solidaire, etc.)
---

# Articles

Des articles sur des sujets qui m’occupent et me préoccupent dans le cadre mon travail (écoconception, accessibilité, économie sociale et solidaire, etc.)

