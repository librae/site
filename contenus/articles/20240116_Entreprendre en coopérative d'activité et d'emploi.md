---
metadonnees:
- description: Depuis 2019, j'entreprends au sein de la Coopérative d'Activité et d'Emploi l'Ouvre-Boîtes. L'article qui suit reprends ma candidature de sociétariat, déposée en 2021.
---

## Aligner ses valeurs et ses compétences

Natif de la fin des années 80, et ayant connu l’arrivée des « Personnal Computer » et des consoles vidéoludiques dans les foyers, j’ai rapidement développé une forte appétence pour le numérique et la culture Geek. 

Mes pérégrinations scolaires, mon attrait pour ce secteur couplés à ma grande curiosité, m’ont amené à vouloir comprendre les intrications des pixels de nos interfaces en programmant, pour mon compte, à l’aide de ma Casio, puis pour autrui, grâce à ma formation scolaire, des outils de gestion afin de faciliter le quotidien et le travail des utilisateur·trice·s de ceux-ci.

Dix-sept années, une rupture conventionnelle, une prise de conscience environnementale et d’innombrables phases de remue-méninges plus tard, je découvrais, par les mots de Raphaël Baer, l’Ouvre-Boîtes 44, son fonctionnement et ses valeurs : mettre ses compétences au service d’une économie sociale et solidaire, centrée sur l’humain et la coopération, tout en bénéficiant d’un accompagnement nous évitant d’être seul.e dans l’infini de l’espace entrepreneurial.

Il n’en fallu pas beaucoup plus pour créer un déclic et entamer la mise en orbite de mon activité.

## Mes choix entreprenariaux

Je souhaite pouvoir exprimer l’ensemble de mes compétences, remettre de l’humanité dans la façon de pratiquer mon métier tout en portant fortement mes valeurs personnelles au travers de mon travail. L’Ouvre-Boîtes 44 me permet de bénéficier d’un accompagnement dans la construction de ce projet d’entreprise tout en m’assurant une situation financière sereine par un statut de salarié.

## Libræ et l'Ouvre-Boîtes

### Un numérique écologique, social et solidaire

Le numérique, et plus spécifiquement le Web, est omniprésent dans notre quotidien et peut se révéler indispensable lorsque l’on porte un projet entrepreneurial, afin de communiquer autour de ses activités, valeurs et services.
Mais le numérique est une ressource critique qu’il convient d’utiliser de manière raisonnable et raisonnée afin d’en limiter les effets sur notre environnement (émissions de gaz à effet de serre, consommation d’eau douce, etc.) et nos sociétés (obsolescence psychologique, fracture numérique, etc.)

### Pourquoi l'Ouvre-Boîtes ?

Lors de ma rupture conventionnelle avec mon précédent employeur, j’avais pour souhait de créer mon entreprise afin de palier le manque ressenti sur mes dernières années d’exercice. Mais l’insécurité face à l’auto-entrepreneuriat
bloquait cette volonté de créer. 

À la suite d’une candidature spontanée auprès d’Eolis (Raphaël Baer), j’ai découvert l’Ouvre-Boîtes et le monde des CAE. Son discours ayant levé mes inquiétudes quant à mon projet, j’ai rejoint la coopérative avec le souhait de mettre mes compétences au service de projets à taille humaine, d’exprimer la créativité que je devais taire en société de service et d’évoluer dans un milieu me permettant de porter librement mes valeurs et mon discours écologique et éthique.

### C’est quoi Libræ ?

Libræ correspond au génitif de Libra, la constellation de la balance en latin. Ce mot à une double signification pour moi : la première, personnelle, est sa référence à mon signe astrologique. La seconde fait référence à la question qui m’anime au quotidien : comment vivre en limitant autant que possible mon impact environnemental et social ?

### Mes engagements professionnels

Ma volonté, à travers mon activité, est d’idéer un numérique moins irresponsable. Je pense qu’il est possible d’inventer un numérique plus écologique, plus sobre, plus paritaire et plus décentralisé afin de permettre à chaque personne de s’approprier ou se réapproprier cet outil pour couvrir ses besoins essentiels. Mais je considère également que défendre le vivant et les libertés humaines implique nécessairement une utilisation frugale du numérique.

Toutes ces valeurs s’incarnent fortement dans mon métier qui se décompose ainsi en deux piliers : écoconcevoir des sites web et sensibiliser aux impacts du numérique.

### Engagements coopératif

1. Développer des collaborations internes avec des entrepreuneur·euse·s de l’Ouvre-Boîtes plus particulièrement au sein du collectif numérique
1. Permettre à la coopérative et aux entrepreuneur·euse·s de se doter d’outils numérique frugals et plus responsables

## Libræ en chiffres

Libræ est accompagnée depuis mai 2019 par l’Ouvre-Boîtes 44. En CAPE jusqu’en février 2020, le rétribution liée à mon activité a régulièrement augmenté pour atteindre 100 % du SMIC en Septembre 2020. 2021 fut la première année complète
à plein temps. L’objectif nominal (SMIC) a été atteint avec les activités ci-dessous :

- Écoconception web
- Cours informatique
- Conférences

### Evolution économique depuis 2019
Exercice | 2019 | 2020 | 2021 
 --- | --- | --- | --- 
Chiffre d'affaire | 3561,59 € | 18281,41 € | 29704,78 €
Charges | 798,22 €  | 5578,60 € | 4034,69 €
Salaires | 2666,00 € | 9980,53 € | 24687,75€

## Remerciements

- Emmanuelle Didierjean
- Raphael Baer
- Le collectif Translucide
- Thierry Rousselot
- Caroline Dewynter
- Pauline Durillon
- Le collectif Numérique de l'Ouvre-Boîtes
- Les coopworkers

## Liens externes
- [Télécharger le dossier complet (au format pdf)](https://nuage.librae.fr/index.php/s/FfTg7tbHDbgHRbX)


