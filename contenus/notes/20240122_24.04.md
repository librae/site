---
metadonnees:
- description: Notes hebdomadaires janvier 2024, semaine 04. J’y partage mes projets du moment, ma veille technologique, mes joies, mes peines et quelques une de mes distractions.
---

## Les projets du moment

Libræ
: La version expérimentale du Système de Gestion de Contenu présentait un bogue dans la génération du chemin vers la feuille de style pour les contenus présents dans des sous-dossiers (comme c'est le cas pour mes [notes hebdo](./)). L'erreur est corrigée, ce qui évite une révision manuelle des fichier HTML (<span lang="en">Hypertext Markup Language</span>) généré.

Entreprises & Biodiversité
: Une semaine de révisions graphiques et fonctionnelles avant la recette clientèle. Certaines fonctionnalités et composants sont recyclées du projet [ENR Durables](https://enrdurables.org/).

La Coliniere
: Nouvelle semaine, nouveau TP: un exercice pratique pour passer d'un site web statique (thème HTML5/CSS3) à un site dynamique (en PHP). 4 niveaux de tehnicité croissants sont disponibles pour les élèves allant du site statique généré en PHP (un script = une page) au site généré par analyse d'URL (via un fichier `.htaccess`). La scéance de cours a été dédiée à la définition d'un projet numérique et à la gestion du temps à l'aide d'un diagramme de Gantt.

## Veille numérique

- Les [retours d'expérience des interventions de L'Etabli numérique](https://letab.li/blog/2024_01_ecolo/) sur l'impact écologique du numérique. J'aime beaucoup l'approche bienveillante de leurs interventions et la prise en compte des aspects sociaux, qui me semble essentielle pour aborder la critique du numérique.
- Le [billet de Louis Derrac sur l'inclusion numérique](https://louisderrac.com/2024/01/comment-nos-villes-peuvent-elles-contribuer-a-un-monde-plus-libre/) visant à apporter une critique de celle-ci. Le point de vue est intéréssant et viens enrichir mes reflexions quand à l'accéssibilité technique de mes développements.
- Le [MAIF Mag](https://entreprise.maif.fr/files/live/sites/entreprise-Maif/files/pdf/maif-mag/maif-mag-194-janvier-2024.pdf) avec un article sur la fin de la carte verte au profit du techno-solutionnisme avec la mise en place d'un fichier des véhicules assurés et la mise en place de caméra connectées sur le bord des routes.
- Toujours dans le [MAIF Mag](https://entreprise.maif.fr/files/live/sites/entreprise-Maif/files/pdf/maif-mag/maif-mag-194-janvier-2024.pdf) dans lequel on peut découvrir que "La réparation d’un téléphone coûte deux fois moins cher que son remplacement par un appareil neuf équivalent".


## Mes joies

- J'ai reçu mon exemplaire d'*Ada et Zangemann*, lu imédiatement. 
- J'ai profité de mon mercredi après-midi pour cuisiner pour la famille. J'ai pris enormement de plaisir à me mettre aux fourneaux.
- Je me suis fait aider par une collègue dans ma trame de cours, ça fait du bien de faire partie d'une équipe pédagogique bienveillante et aidante.
- J'ai pu enfin faire réparer mon casque audio grâce 

## Mes peines

- Mes cours ont été obnubilant et source de frustration cette semaine. J'ai l'impression de ne pas réussir à créer le rapport éducatif que je voudrais avec mes élèves, de pas intéresser et cela m'a questionné sur ma légitimité.

## J'ai lu, vu ou écouté

-  <i aria-hidden="true">🎧</i>  Écouté *Murmures Nomades* de Björn Gottschall. Acheté il y a quelques mois lors d'un week-end à Saint-Malo. Un album de musique au piano qui est une caresse pour mes oreilles.
- <i aria-hidden="true">📚</i> <span lang="en">*Universal War Two*</span> (tome 1, *Le temps du désert*) de Denis Bajram dans lequel l'humanité fait face à une nouvelle menace, une dizaine d'année après la première guerre universelle. La première série était bien traitée et j'ai hâte de découvrir sa suite.
- <i aria-hidden="true">📚</i> *Ada et Zangemann* de Matthias Kirschner et Sandra Brandstätter, nous racontant l'histoire d'Ada, jeune fille bricoleuse qui découvre et apprivoise le monde numérique (matériel et logiciel). Un album jeunesse parlant de code, de logiciel libre et d'éducation au numérique, il n'en fallait guère plus pour me séduire.
