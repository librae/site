---
metadonnees:
- description: Notes hebdomadaires. J’y partage mes projets du moment, ma veille technologique, mes joies, mes peines et quelques une de mes distractions.
---

Lorem ipsum odor amet, consectetuer adipiscing elit. Ut accumsan fermentum montes fusce curae imperdiet. Venenatis nisi commodo tortor, arcu at netus aenean.

## En ce moment

Consectetuer
: Lorem ipsum odor amet, consectetuer adipiscing elit.

## Veille numérique

- [](){target="_blank"}.
- [](){target="_blank"}.

## Mes joies

- Lorem ipsum odor amet, consectetuer adipiscing elit.

## Mes peines

- Lorem ipsum odor amet, consectetuer adipiscing elit.

## Lu, vu ou écouté

- <i aria-hidden="true">🎧</i> [Podcast](){target="_blank"}.
- <i aria-hidden="true">📺</i> [Télévision](){target="_blank"}. 
- <i aria-hidden="true">📚</i> [Lecture](){target="_blank"}.
- <i aria-hidden="true">🎞️</i> [Cinema](){target="_blank"}.
- <i aria-hidden="true">🎭</i> [Théatre](){target="_blank"}.
- <i aria-hidden="true">🏛️</i> [Musée](){target="_blank"}.