---
metadonnees:
- description: Notes hebdomadaires. J’y partage mes projets du moment, ma veille technologique, mes joies, mes peines et quelques une de mes distractions.
---

# Notes hebdo 25.08

## En ce moment

La Colinière
: Ce sont les vacances.
: Projet *ministages*
    : J'expériemente du code afin d'avoir que celui-ci soit facile à lire et à comprendre.
    : Je fait beaucoup de *R&D* autour de l'achitecture logicielle en couches afin de trouver un compromis entre normes de développement et écoconception (espace de stockage, usage mémoire, etc.)

Libræ
: Je travaille sur le plan d'un workshop autour de l'écoconception et de l'accessibilité numérique.

HB Correction
: Je travaille sur une nouvelle version graphique du site afin d'améliorer sa dynamique.

RAFUE
: Je participe à une visio pour répondre aux questions concernant la création et la gestion de contenus dans le [*CMS*]{lang="en"} Translucide.

Entreprise et Biodiversité
: Je corrige une anomalie au niveau d'un formulaire du site proposant une option inadéquate dans le contexte.

## Veille technologique

- [Captcha et accessibilité : Les personnes handicapées ne sont pas des robots ! [(nouvelle fenêtre)]{class="sr-only"}](https://design.numerique.gouv.fr/articles/2024-11-28-captcha-et-accessibilite/){target="blank"}. Ou j'apprends qu'il vaut mieux opter pour des captcha simple pour garantir l'accessibilité
- [La loi pour l’accessibilité numérique a 20 ans… [(nouvelle fenêtre)]{class="sr-only"}](https://www.lalutineduweb.fr/loi-accessibilite-numerique-20-ans/){target="blank"}. Un article dans lequel Julie Moynat fait le point sur l'anniversaire de l'accessibilité numérique.
- Je lis beaucoup de documentation autour du développement en couche ([[*DDD*]{lang="en"} [(nouvelle fenêtre)]{class="sr-only"}](https://en.wikipedia.org/wiki/Domain-driven_design){target="_blank"}, [[*Multitier architecture*]{lang="en"} [(nouvelle fenêtre)]{class="sr-only"}](https://en.wikipedia.org/wiki/Multitier_architecture){target="blank"})

## Mes joies

- C'est un bonheur d'avoir adopté Shirley. Elle semble fortement se plaire avec nous à la maison.

## Mes peines

- Shirley semble souffrir d'anxiété sociale.

## Lu, vu ou écouté

- [📺]{aria-hidden="true"} [On a Tout Perdu... en 2h (dans une inondation) [(nouvelle fenêtre)]{class="sr-only"}](https://www.youtube.com/watch?v=Wj7pq8HAEW4){target="blank"}. Une vidéo de Hihack qui documente l'innondation qu'il a vécu et la perte de ses biens. Touchante d'un coté, je trouve que sa réaction finale montre bien la dissonance cognitive qui perciste dans nos esprits entre consommation de masse et dérèglement climatique.
- [📰]{aria-hidden="true"} [La consommation mondiale d’électricité pourrait augmenter de 4 % par an jusqu’en 2027 [(nouvelle fenêtre)]{class="sr-only"}](https://reporterre.net/La-consommation-mondiale-d-electricite-pourrait-augmenter-de-4-par-an-jusqu-en-2027){target="blank"}.
- [📺]{aria-hidden="true"} [*Les Gardiens de la Galaxie Vol. 3* [(nouvelle fenêtre)]{class="sr-only"}](https://fr.wikipedia.org/wiki/Les_Gardiens_de_la_Galaxie_Vol._3){target="blank"}. Le film se révèle divertissant mais sans plus.
- [📺]{aria-hidden="true"} [*Les Tuche 3: Liberté, Égalité, Fraternituche* [(nouvelle fenêtre)]{class="sr-only"}](https://fr.wikipedia.org/wiki/Les_Tuche_3:_Libert%C3%A9,_%C3%89galit%C3%A9,_Fraternituche){target="blank"}. 3ème volet des Tuche, l'humour est toujours au rendez-vous et les boutades efficaces.
- [📚]{aria-hidden="true"} [*One Piece* 1140 [(nouvelle fenêtre)]{class="sr-only"}](https://mangaplus.shueisha.co.jp/titles/100020){target="_blank"}. La suite des célèbres aventures de Luffy et de son équipage sur les mers de [*grand line*]{lang="en"}
- [📚]{aria-hidden="true"} [*Le langage du chien* [(nouvelle fenêtre)]{class="sr-only"}](https://hariet-et-rosie.com/products/le-language-du-chien?_pos=1&_sid=714f5da4c&_ss=r){target="_blank"} de Lili Chin. Un bel ouvrage illustré sur le langage corporel canin.
- [📺]{aria-hidden="true"} [*Gagné ou perdue* [(nouvelle fenêtre)]{class="sr-only"}](Gagné ou perdu){target="blank"}. La première série de Pixar dans laquelle chaque épisode est consacrée au vécu d'un personne la semaine précédent un championnat de baseball. On y retrouve le sel de la franchise, des protagonistes devant faire face au monde dans lequel iels vivent.