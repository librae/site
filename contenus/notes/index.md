---
metadonnees:
- description: Mes notes hebdomadaires. J’y partage mes projets du moment, ma veille technologique, mes joies, mes peines et quelques-unes de mes distractions.
---

# Notes hebdo

Mes notes hebdomadaires. J’y partage mes projets du moment, ma veille technologique, mes joies, mes peines et quelques-unes de mes distractions.

