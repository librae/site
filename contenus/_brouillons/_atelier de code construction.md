---
template: article
robots: noindex,nofollow
title: Atelier de co(de)-construction
metadonnees:
- description:  
chapo: Cet atelier à pour ambition de ... 
---

Depuis mes débuts dans le domaine de l'écoconception numérique en 2019, ma vision de ce qu'est cette discipline à beaucoup évoluée.  Aujourd'hui, la prestation de conception web basée sur le SGC (Système de Gestion de Contenu) [Translucide](https://translucide.net) ne me permet pas de répondre à l'ensemble des bonnes pratiques de l'écoconception. En effet, la première de celles-ci étant l'élimination des fonctionnalités non essentielles, la mise en place d'un SGC devrait être également questionnée. Même Translucide s'avère être un outil très accessible pour sa gestion de contenu, celui-ci ne s'avère, à mon sens, pas adapté à l'ensemble des projets pour lesquels on m'accorde sa confiance.

De cette réflexion, je propose, depuis 2022 le développement de sites statiques (sans interface de gestion de contenu) mono-page en vue d'éviter la mise en place d'un environnement numérique plus impactant économiquement (mise en place, maintenance, etc.) pour une "simple" plaquette numérique. 

Néanmoins, il existe, entre ces deux solutions, un vide que j'aimerais combler : permettre la mise en place de sites statiques tout en ayant la main sur son contenu.

Un projet a donc germé de mes réflexions et de mes aspirations : développer un outil de gestion de contenu avec la couverture fonctionnelle et technique la plus sobre possible afin qu'il puisse est pris en main par des néophytes. 

Cet atelier de co(de)-construction s'incrit dans une volonté de donner la parole à des personnes éloignées de la programmation, voire du numérique, afin de vous acculturer aux notions d'algorithmie et de développement afin de construire un outil s'inscrivant dans l'esprit de communs numérique.

