---
metadonnees:
- description: Imaginer un numérique respectueux des enjeux environnementaux et sociaux.
---

# Imaginer un numérique respectueux des enjeux environnementaux et sociaux.

1. **Écoconçeption et accessiblilité web** <br>*Concevoir et développer des sites web à votre image et à moindre impact.*
2. **Alternatives numériques** <br>*Imaginer et développer des outils numériques sobres et efficients adaptés aux utilisateurices.*
3. **Ateliers et formations** <br>*Sensibiliser et éduquer au numérique et à ses impacts.*

> Pour un avenir soutenable, nous devons radicalement repenser ce dont nous avons vraiment besoin !

Libræ défends une approche artisanale, sobre et inclusive du web pour un numérique écoconçu, accessible, paritaire et local pour permettre à chaque personne de s'approprier ou se réapproprier cet outil. 

[*Envoyer un courriel*](mailto:mathieu@librae.fr?subject=%5Blibræ.fr%5D%20Travaillons%20ensemble%20!&body=Bonjour%20Mathieu%20!%0A%0A){title="Envoyer un courriel - ouverture dans la boîte mail" target="_self"}