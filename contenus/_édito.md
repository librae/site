---
metadonnees:
- description: Création de sites web à Nantes. Libræ applique les bonnes pratiques d'écoconception et d'accessibilité pour un web moins irresponsable.
---

## Le numérique contribut au dérèglement climatique, à l'aliénation sociale et à l'invisibilisation des minorités.

Les emissions de gaz à effet de serre du secteur numérique sont de l'ordre de [**1400 millions de tonnes de gaz à effet de serre**](https://www.greenit.fr/etude-empreinte-environnementale-du-numerique-mondial/ "Lire l'étude GreenIT : Empreinte environnementale du numérique mondial (Nouvel onglet)"){target="_blank"}.

En France, ce sont [**11,526 millions de personnes qui sont concernées par l'illectronisme**](https://sps.fr/wp-content/uploads/2020/01/SPS_Livre_Blanc_Contre_Illectronisme_Octobre2019.pdf "Livre blanc contre l'illectronisme (Nouvel onglet. Document PDF)"){target="_blank"}.

Les algorithmes contribuent à l'[**invisibilisation des Femmes, des personnes Trans et des personnes Racisées**](https://www.vie-publique.fr/en-bref/274595-algorithmes-alerte-sur-les-risques-de-discriminations "Algorithmes : prévenir les risques de discriminations (Nouvel onglet)"){target="_blank"}.

L'exploitation et l'extraction des terres rares entretiennent le [**colonialisme et travail de 40000 enfants**](https://www.amnesty.org/fr/latest/news/2016/01/child-labour-behind-smart-phone-and-electric-car-batteries/ "Le travail des enfants derrière la production de smartphones et de voitures électriques (Nouvel onglet)"){target="_blank"}.

## On ne sauvera pas le monde en le numérisant ! 

**Il est temps de questionner nos besoins Numériques&nbsp;!** Chaque octect à un impact sur le monde réel et le poids médian des pages web ne cesse de croitre.

Aujourd’hui, **la numérisation à outrance présente plus d’aspects négatifs que positifs** et ne permet pas de répondre aux enjeux environnementaux et de droits humains.

Souhaitons-nous un Numérique toujours plus omniprésent, dogmatique et impactant ?

**Vous n’avez pas besoin d’un site WordPress ou Wix**. Ces [inflagiciels "Définition d'inflagiciel (Nouvel onglet)"](https://fr.wikipedia.org/wiki/Bloatware){target="_blank"} vous garantissent un site plein de fonctionnalités inutiles, de pages lourdes et énergivores, et une expérience de navigation inconfortable.

## Libræ s'investit en faveur d'un numérique désirresponsable. 

Depuis 2019, Libræ s'investi dans la réduction des impacts du Numériques. En intégrant les pratiques d’écoconception et d'accessibilité dans une approche sobre et frugale du web, il est possible de créer des sites léger, efficients et à moindre impacts.
