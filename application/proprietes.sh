#! /bin/bash

# Ce script contient l'ensemble des fonctions permettant de gérer les propriétés des dossier et fichiers
# du système de gestion de contenu

## Retourne la date de création d'un dossier ou d'un fichier
function date_creation {
   if [[ -d $1 || -f $1 ]]
   then echo $(stat -c %w "$1" | grep -oE '[0-9]{4}-[0-9]{2}-[0-9]{2}' )
   fi
}

## Retourne la date de création d'un dossier ou d'un fichier
function date_modification {
   if [[ -d $1 || -f $1 ]]
   then echo $(stat -c %y "$1" | grep -oE '[0-9]{4}-[0-9]{2}-[0-9]{2}' ) 
   fi
}

## transforme une date technique en date FR 
function date_FR {

   mois=(janv. févr. mars. avr. mai juin juill. août sept. oct. nov. déc.)

   aaaa=$(echo $1 | sed -E 's/([0-9]{4})-([0-9]{2})-([0-9]{2})/\1/')
   mmm=$(echo $1 | sed -E 's/([0-9]{4})-([0-9]{2})-([0-9]{2})/\2/')   
   jj=$(echo $1 | sed -E 's/([0-9]{4})-([0-9]{2})-([0-9]{2})/\3/')
   
   echo $jj ${mois[$mmm-1]} $aaaa;
}